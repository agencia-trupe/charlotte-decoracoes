<?php

use \Contato, \Cadastro;

class ContatoController extends BaseController {

	protected $layout = 'frontend.templates.index';

	public function index()
	{
		$this->layout->content = View::make('frontend.contato.index')->with('contato', Contato::first());
	}

	public function enviar()
	{
		$data['nome'] = Input::get('nome');
		$data['email'] = Input::get('email');
		$data['telefone'] = Input::get('telefone');
		$data['mensagem'] = Input::get('mensagem');

		if($data['nome'] && $data['email'] && $data['mensagem']){
			Mail::send('emails.contato', $data, function($message) use ($data)
			{
			    $message->to('sac@charlottemoveis.com.br', 'Charlotte Movelaria')
			    		->subject('Contato via site')
			    		->replyTo($data['email'], $data['nome']);
			});
		}
		return 'Email enviado com sucesso! Responderemos assim que possível.';
	}

	public function newsletter()
	{
		$nome = Input::get('nome');
		$email = Input::get('email');

		if(!$nome) return 'Informe seu nome!';
		if(!$email) return 'Informe seu e-mail!';

		$test_email = Cadastro::where('email', '=', $email)->get();
		if(sizeof($test_email) > 0){
			return 'O email já está cadastrado!';
		}else{
			$cad = new Cadastro;
			$cad->nome = $nome;
			$cad->email = $email;
			$cad->save();
			return 'Obrigado! Email cadastrado com sucesso!';
		}
	}

}

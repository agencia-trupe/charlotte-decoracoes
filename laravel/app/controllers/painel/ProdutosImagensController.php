<?php

namespace Painel;

use \View, \Input, \Str, \Session, \Redirect, \Hash, \Thumb, \File, \Produto, \ProdutosImagem;

class ProdutosImagensController extends BaseAdminController {

	protected $layout = 'backend.templates.index';

	protected $limiteInsercao = false;

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$produtos_id = Input::get('produtos_id');

		if(!$produtos_id)
			return Redirect::back();

		$this->layout->content = View::make('backend.produtosimagens.index')->with('registros', ProdutosImagem::orderBy('ordem', 'ASC')->where('produtos_id', $produtos_id)->get())
																			->with('produto', Produto::find($produtos_id));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$produtos_id = Input::get('produtos_id');

		if(!$produtos_id)
			return Redirect::back();

		$this->layout->content = View::make('backend.produtosimagens.form')->with('produto', Produto::find($produtos_id));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$object = new ProdutosImagem;

		$object->produtos_id = Input::get('produtos_id');

		$t=date('YmdHis');
		$imagem = Thumb::make('imagem', 55, 55, 'produtosimagens/thumbs/', $t);
		Thumb::make('imagem', 940, null, 'produtosimagens/', $t);
		if($imagem) $object->imagem = $imagem;

		try {

			$object->save();

			Session::flash('sucesso', true);
			Session::flash('mensagem', 'Imagem criado com sucesso.');
			return Redirect::route('painel.produtosimagens.index', array('produtos_id' => $object->produtos_id));

		} catch (\Exception $e) {

			Session::flash('formulario', Input::all());
			return Redirect::back()->withErrors(array('Erro ao criar Imagem!'));	

		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$imagem = ProdutosImagem::find($id);

		$produto = Produto::find($imagem->produtos_id);

		$this->layout->content = View::make('backend.produtosimagens.edit')->with('registro', $imagem)
																		   ->with('produto', $produto);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$object = ProdutosImagem::find($id);

		$object->produtos_id = Input::get('produtos_id');

		$t=date('YmdHis');
		$imagem = Thumb::make('imagem', 55, 55, 'produtosimagens/thumbs/', $t);
		Thumb::make('imagem', 940, null, 'produtosimagens/', $t);
		if($imagem) $object->imagem = $imagem;

		try {

			$object->save();
			Session::flash('sucesso', true);
			Session::flash('mensagem', 'Imagem alterado com sucesso.');
			return Redirect::route('painel.produtosimagens.index', array('produtos_id' => $object->produtos_id));

		} catch (\Exception $e) {

			Session::flash('formulario', Input::all());
			return Redirect::back()->withErrors(array('Erro ao criar Imagem!'));

		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$object = ProdutosImagem::find($id);
		$object->delete();

		Session::flash('sucesso', true);
		Session::flash('mensagem', 'Imagem removido com sucesso.');

		return Redirect::route('painel.produtosimagens.index');
	}

}
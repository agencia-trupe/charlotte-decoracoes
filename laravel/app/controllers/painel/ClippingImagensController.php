<?php

namespace Painel;

use \View, \Input, \Str, \Session, \Redirect, \Hash, \Thumb, \File, \Clipping, \ClippingsImagem;

class ClippingImagensController extends BaseAdminController {

	protected $layout = 'backend.templates.index';

	protected $limiteInsercao = false;

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$clipping_id = Input::get('clipping_id');

		if(!$clipping_id)
			return Redirect::back();

		$this->layout->content = View::make('backend.clippingimagens.index')->with('registros', ClippingsImagem::where('clippings_id', '=', $clipping_id)->orderBy('ordem', 'ASC')->get())
																			->with('clipping', Clipping::find($clipping_id));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$clipping_id = Input::get('clipping_id');

		if(!$clipping_id)
			return Redirect::back();

		$this->layout->content = View::make('backend.clippingimagens.form')->with('clipping', Clipping::find($clipping_id));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$object = new ClippingsImagem;

		$clipping_id = Input::get('clipping_id');
		$object->clippings_id = $clipping_id;

		$t=date('YmdHis');
		$imagem = Thumb::make('imagem', 200, 300, 'clippingimagens/thumbs/', $t);
		Thumb::make('imagem', 600, null, 'clippingimagens/', $t);
		if($imagem) $object->imagem = $imagem;

		try {

			$object->save();

			Session::flash('sucesso', true);
			Session::flash('mensagem', 'Imagem criada com sucesso.');
			return Redirect::route('painel.clippingimagens.index', array('clipping_id' => $clipping_id));

		} catch (\Exception $e) {

			Session::flash('formulario', Input::all());
			return Redirect::back()->withErrors(array('Erro ao criar Imagem!'));	

		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$registro = ClippingsImagem::find($id);

		$clipping = Clipping::find($registro->clipping_id);

		$this->layout->content = View::make('backend.clippingimagens.edit')->with('registro', $registro)
																			->with('clipping', $clipping);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$object = ClippingsImagem::find($id);

		$clipping_id = Input::get('clipping_id');

		$t=date('YmdHis');
		$imagem = Thumb::make('imagem', 200, 300, 'clippingimagens/thumbs/', $t);
		Thumb::make('imagem', 600, null, 'clippingimagens/', $t);
		if($imagem) $object->imagem = $imagem;
		
		try {

			$object->save();
			Session::flash('sucesso', true);
			Session::flash('mensagem', 'Imagem alterada com sucesso.');
			return Redirect::route('painel.clippingimagens.index', array('clipping_id' => $clipping_id));

		} catch (\Exception $e) {

			Session::flash('formulario', Input::all());
			return Redirect::back()->withErrors(array('Erro ao criar Imagem!'));

		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$object = ClippingsImagem::find($id);

		$clipping_id = $object->clipping_id;

		$object->delete();

		Session::flash('sucesso', true);
		Session::flash('mensagem', 'Imagem removida com sucesso.');

		return Redirect::route('painel.clippingimagens.index', array('clipping_id' => $clipping_id));
	}

}
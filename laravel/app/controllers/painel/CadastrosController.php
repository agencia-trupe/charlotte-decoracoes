<?php

namespace Painel;

use \View, \Input, \Str, \Session, \Redirect, \Hash, \Thumb, \File, \Cadastro;

class CadastrosController extends BaseAdminController {

	protected $layout = 'backend.templates.index';

	protected $limiteInsercao = false;

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$this->layout->content = View::make('backend.cadastros.index')->with('registros', Cadastro::orderBy('nome', 'asc')->get());		
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{		
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{		
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$object = Cadastro::find($id);
		$object->delete();

		Session::flash('sucesso', true);
		Session::flash('mensagem', 'Cadastro removido com sucesso.');

		return Redirect::route('painel.cadastros.index');
	}

	public function download()
	{
    	$resultados = Cadastro::orderBy('nome')->select('nome', 'email')->get()->toArray();

    	$pathToFile = app_path() . '/internal_files/cadastros_'.date('d-m-Y-H-i-s').'.xls';	    	

    	$titulos = array(
			'nome' => 'Nome',
            'email' => 'E-mail'
	    );
    	array_unshift($resultados, $titulos);
		Excel::fromArray($resultados)->save($pathToFile);
		return Response::download($pathToFile);
	}
}
<?php

namespace Painel;

use \View, \Input, \Str, \Session, \Redirect, \Hash, \Thumb, \File, \Produto, \Categoria, \Destaque, \Lancamento;

class ProdutosController extends BaseAdminController {

	protected $layout = 'backend.templates.index';

	protected $limiteInsercao = false;

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$categorias_id = Input::get('categorias_id');
		$categoria = Categoria::find($categorias_id);

		if(is_null($categoria)){
			$produtos = Produto::orderBy('ordem', 'ASC')->get();
		}else{
			$produtos = $categoria->produtos;
		}

		$this->layout->content = View::make('backend.produtos.index')->with('registros', $produtos)
																	 ->with('categorias', Categoria::orderBy('ordem', 'ASC')->get())
																	 ->with('categoria', $categoria);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$this->layout->content = View::make('backend.produtos.form')->with('categorias', Categoria::orderBy('titulo', 'asc')->get());
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$object = new Produto;

		$object->prefixo = Input::get('prefixo');
		$object->titulo = Input::get('titulo');
		$object->produtos_categorias_id = Input::get('produtos_categorias_id');

		$t=date('YmdHis');
		$imagem = Thumb::make('imagem', 200, 200, 'produtos/thumbs/', $t, '#FFFFFF', false);
		Thumb::make('imagem', 450, 450, 'produtos/', $t, '#FFFFFF', false);
		if($imagem) $object->imagem = $imagem;

		try {

			$object->save();
			$object->slug = Str::slug($object->id.'-'.$object->prefixo.'-'.$object->titulo);
			$object->save();

			if(Input::has('lancamento') && Input::get('lancamento') == 1){
				$lancamento = new Lancamento;
				$lancamento->produtos_id = $object->id;
				$lancamento->save();
			}

			Session::flash('sucesso', true);
			Session::flash('mensagem', 'Produto criado com sucesso.');
			return Redirect::route('painel.produtos.index', array('categorias_id' => $object->produtos_categorias_id));

		} catch (\Exception $e) {

			Session::flash('formulario', Input::all());
			return Redirect::back()->withErrors(array('Erro ao criar Produto!'));	

		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$this->layout->content = View::make('backend.produtos.edit')->with('registro', Produto::find($id))
																	->with('categorias', Categoria::orderBy('titulo', 'asc')->get());
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$object = Produto::find($id);

		$object->prefixo = Input::get('prefixo');
		$object->titulo = Input::get('titulo');
		$object->produtos_categorias_id = Input::get('produtos_categorias_id');
		
		$t=date('YmdHis');
		$imagem = Thumb::make('imagem', 200, 200, 'produtos/thumbs/', $t, '#FFFFFF', false);
		Thumb::make('imagem', 450, 450, 'produtos/', $t, '#FFFFFF', false);
		if($imagem) $object->imagem = $imagem;

		try {

			$object->save();
			$object->slug = Str::slug($object->id.'-'.$object->prefixo.'-'.$object->titulo);
			$object->save();
			
			Session::flash('sucesso', true);
			Session::flash('mensagem', 'Produto alterado com sucesso.');
			return Redirect::route('painel.produtos.index', array('categorias_id' => $object->produtos_categorias_id));

		} catch (\Exception $e) {

			Session::flash('formulario', Input::all());
			return Redirect::back()->withErrors(array('Erro ao criar Produto!'));

		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		Destaque::where('produtos_id', '=', $id)->delete();
		Lancamento::where('produtos_id', '=', $id)->delete();
		\ProdutosImagem::where('produtos_id', '=', $id)->delete();

		$object = Produto::find($id);

		$cat_id = $object->produtos_categorias_id;

		$object->delete();

		Session::flash('sucesso', true);
		Session::flash('mensagem', 'Produto removido com sucesso.');

		return Redirect::route('painel.produtos.index', array('categorias_id' => $cat_id));
	}

}
<?php

namespace Painel;

use \View, \Input, \Str, \Session, \Redirect, \Hash, \Tools, \Thumb, \File, \Clipping;

class ClippingsController extends BaseAdminController {

	protected $layout = 'backend.templates.index';

	protected $limiteInsercao = false;

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$this->layout->content = View::make('backend.clippings.index')->with('registros', Clipping::orderBy('data', 'DESC')->get());		
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$this->layout->content = View::make('backend.clippings.form');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$object = new Clipping;

		$object->titulo = Input::get('titulo');
		$object->data = Tools::converteData(Input::get('data'));

		$imagem = Thumb::make('imagem', 215, 280, 'clippings/');
		if($imagem) $object->imagem = $imagem;		

		try {

			$object->save();

			Session::flash('sucesso', true);
			Session::flash('mensagem', 'Clipping criado com sucesso.');
			return Redirect::route('painel.clippings.index');

		} catch (\Exception $e) {

			Session::flash('formulario', Input::all());
			return Redirect::back()->withErrors(array('Erro ao criar Clipping!'));	

		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$this->layout->content = View::make('backend.clippings.edit')->with('registro', Clipping::find($id));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$object = Clipping::find($id);

		$object->titulo = Input::get('titulo');
		$object->data = Tools::converteData(Input::get('data'));

		$imagem = Thumb::make('imagem', 215, 280, 'clippings/');
		if($imagem) $object->imagem = $imagem;

		try {

			$object->save();
			Session::flash('sucesso', true);
			Session::flash('mensagem', 'Clipping alterado com sucesso.');
			return Redirect::route('painel.clippings.index');

		} catch (\Exception $e) {

			Session::flash('formulario', Input::all());
			return Redirect::back()->withErrors(array('Erro ao criar Clipping!'));

		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$object = Clipping::find($id);

		$object->paginas()->delete();

		$object->delete();

		Session::flash('sucesso', true);
		Session::flash('mensagem', 'Clipping removido com sucesso.');

		return Redirect::route('painel.clippings.index');
	}

}
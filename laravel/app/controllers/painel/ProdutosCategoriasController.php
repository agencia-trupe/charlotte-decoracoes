<?php

namespace Painel;

use \View, \Input, \Str, \Session, \Redirect, \Hash, \Thumb, \File, \Categoria;
use \Destaque,
\Lancamento,
\ProdutosImagem;

class ProdutosCategoriasController extends BaseAdminController {

	protected $layout = 'backend.templates.index';

	protected $limiteInsercao = false;

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$this->layout->content = View::make('backend.produtoscategorias.index')->with('registros', Categoria::orderBy('ordem', 'ASC')->get());		
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$this->layout->content = View::make('backend.produtoscategorias.form');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$object = new Categoria;

		$object->titulo = Input::get('titulo');
		$object->slug = Str::slug($object->titulo);

		if(!is_null(Categoria::where('slug', '=', $object->slug)->first())){
			Session::flash('formulario', Input::all());
			return Redirect::back()->withErrors(array('A categoria já existe'));
		}

		try {

			$object->save();

			Session::flash('sucesso', true);
			Session::flash('mensagem', 'Categoria criada com sucesso.');
			return Redirect::route('painel.produtoscategorias.index');

		} catch (\Exception $e) {

			Session::flash('formulario', Input::all());
			return Redirect::back()->withErrors(array('Erro ao criar Categoria!'));	

		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$this->layout->content = View::make('backend.produtoscategorias.edit')->with('registro', Categoria::find($id));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$object = Categoria::find($id);

		$object->titulo = Input::get('titulo');
		$object->slug = Str::slug($object->titulo);

		if(!is_null(Categoria::where('slug', '=', $object->slug)->first())){
			Session::flash('formulario', Input::all());
			return Redirect::back()->withErrors(array('A categoria já existe'));
		}

		try {

			$object->save();
			Session::flash('sucesso', true);
			Session::flash('mensagem', 'Categoria alterada com sucesso.');
			return Redirect::route('painel.produtoscategorias.index');

		} catch (\Exception $e) {

			Session::flash('formulario', Input::all());
			return Redirect::back()->withErrors(array('Erro ao criar Categoria!'));

		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$object = Categoria::find($id);
		

		$produtos = $object->produtos;

		foreach ($produtos as $key => $value) {
			Destaque::where('produtos_id', '=', $value->id)->delete();
			Lancamento::where('produtos_id', '=', $value->id)->delete();
			ProdutosImagem::where('produtos_id', '=', $value->id)->delete();
			$value->delete();
		}

		$object->delete();

		Session::flash('sucesso', true);
		Session::flash('mensagem', 'Categoria removida com sucesso.');

		return Redirect::route('painel.produtoscategorias.index');
	}

}
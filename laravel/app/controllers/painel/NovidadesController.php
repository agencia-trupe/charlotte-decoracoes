<?php

namespace Painel;

use \View, \Input, \Str, \Session, \Redirect, \Hash, \Thumb, \File, \Tools, \Novidade;

class NovidadesController extends BaseAdminController {

	protected $layout = 'backend.templates.index';

	protected $limiteInsercao = false;

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$this->layout->content = View::make('backend.novidades.index')->with('registros', Novidade::orderBy('data', 'DESC')->get());		
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$this->layout->content = View::make('backend.novidades.form');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$object = new Novidade;

		$object->titulo = Input::get('titulo');
		$object->data = Tools::converteData(Input::get('data'));
		$object->texto = Input::get('texto');

		try {

			$object->save();

			Session::flash('sucesso', true);
			Session::flash('mensagem', 'Novidade criada com sucesso.');
			return Redirect::route('painel.novidades.index');

		} catch (\Exception $e) {

			Session::flash('formulario', Input::all());
			return Redirect::back()->withErrors(array('Erro ao criar Novidade!'));	

		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$this->layout->content = View::make('backend.novidades.edit')->with('registro', Novidade::find($id));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$object = Novidade::find($id);

		$object->titulo = Input::get('titulo');
		$object->data = Tools::converteData(Input::get('data'));
		$object->texto = Input::get('texto');

		try {

			$object->save();
			Session::flash('sucesso', true);
			Session::flash('mensagem', 'Novidade alterada com sucesso.');
			return Redirect::route('painel.novidades.index');

		} catch (\Exception $e) {

			Session::flash('formulario', Input::all());
			return Redirect::back()->withErrors(array('Erro ao criar Novidade!'));

		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$object = Novidade::find($id);
		$object->delete();

		Session::flash('sucesso', true);
		Session::flash('mensagem', 'Novidade removida com sucesso.');

		return Redirect::route('painel.novidades.index');
	}

}
<?php

namespace Painel;

use \View, \Input, \Str, \DB, \Session, \Redirect, \Hash, \Thumb, \File, \Destaque, \Produto;

class DestaquesController extends BaseAdminController {

	protected $layout = 'backend.templates.index';

	public $limiteInsercao = '4';

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$this->layout->content = View::make('backend.destaques.index')->with('registros', Destaque::orderBy('ordem', 'ASC')->get())->with('limiteInsercao', $this->limiteInsercao);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		if(count(Destaque::all()) > 0)
			$produtos = DB::table('produtos')->whereNotIn('id', DB::table('destaques')->lists('produtos_id'))->get();
		else
			$produtos = Produto::orderBy('titulo')->get();

		$this->layout->content = View::make('backend.destaques.form')->with('produtos', $produtos);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$object = new Destaque;

		$object->produtos_id = Input::get('produtos_id');

		if($this->limiteInsercao && sizeof(Destaque::all()) >= $this->limiteInsercao)
			return Redirect::back()->withErrors(array('Número máximo de Registros atingido!'));

		try {

			$object->save();

			Session::flash('sucesso', true);
			Session::flash('mensagem', 'Destaque criado com sucesso.');
			return Redirect::route('painel.destaques.index');

		} catch (\Exception $e) {

			Session::flash('formulario', Input::all());
			return Redirect::back()->withErrors(array('Erro ao criar Destaque!'));	

		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		if(count(Destaque::all()) > 0)
			$produtos = DB::table('produtos')->whereNotIn('id', DB::table('destaques')->lists('produtos_id'))->get();
		else
			$produtos = Produto::orderBy('titulo')->get();
					
		$this->layout->content = View::make('backend.destaques.edit')->with('registro', Destaque::find($id))->with('produtos', $produtos);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$object = Destaque::find($id);

		$object->produtos_id = Input::get('produtos_id');

	
		try {

			$object->save();
			Session::flash('sucesso', true);
			Session::flash('mensagem', 'Destaque alterado com sucesso.');
			return Redirect::route('painel.destaques.index');

		} catch (\Exception $e) {

			Session::flash('formulario', Input::all());
			return Redirect::back()->withErrors(array('Erro ao criar Destaque!'));

		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$object = Destaque::find($id);
		$object->delete();

		Session::flash('sucesso', true);
		Session::flash('mensagem', 'Destaque removido com sucesso.');

		return Redirect::route('painel.destaques.index');
	}

}
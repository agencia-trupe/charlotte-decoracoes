<?php

namespace Painel;

use \View, \Input, \Str, \DB, \Session, \Redirect, \Hash, \Thumb, \File, \Produto, \Lancamento;

class LancamentosController extends BaseAdminController {

	protected $layout = 'backend.templates.index';

	protected $limiteInsercao = false;

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$this->layout->content = View::make('backend.lancamentos.index')->with('registros', Lancamento::orderBy('ordem', 'ASC')->get());		
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		if(count(Lancamento::all()) > 0)
			$produtos = DB::table('produtos')->whereNotIn('id', DB::table('lancamentos')->lists('produtos_id'))->get();
		else
			$produtos = Produto::orderBy('titulo')->get();

		$this->layout->content = View::make('backend.lancamentos.form')->with('produtos', $produtos);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$object = new Lancamento;

		$object->produtos_id = Input::get('produtos_id');

		try {

			$object->save();

			Session::flash('sucesso', true);
			Session::flash('mensagem', 'Lançamento criado com sucesso.');
			return Redirect::route('painel.lancamentos.index');

		} catch (\Exception $e) {

			Session::flash('formulario', Input::all());
			return Redirect::back()->withErrors(array('Erro ao criar Lançamento!'));	

		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		if(count(Lancamento::all()) > 0)
			$produtos = DB::table('produtos')->whereNotIn('id', DB::table('lancamentos')->lists('produtos_id'))->get();
		else
			$produtos = Produto::orderBy('titulo')->get();

		$this->layout->content = View::make('backend.lancamentos.edit')->with('registro', Lancamento::find($id))->with('produtos', $produtos);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$object = Lancamento::find($id);

		$object->produtos_id = Input::get('produtos_id');

		try {

			$object->save();
			Session::flash('sucesso', true);
			Session::flash('mensagem', 'Lançamento alterado com sucesso.');
			return Redirect::route('painel.lancamentos.index');

		} catch (\Exception $e) {

			Session::flash('formulario', Input::all());
			return Redirect::back()->withErrors(array('Erro ao criar Lançamento!'));

		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$object = Lancamento::find($id);
		$object->delete();

		Session::flash('sucesso', true);
		Session::flash('mensagem', 'Lançamento removido com sucesso.');

		return Redirect::route('painel.lancamentos.index');
	}

}
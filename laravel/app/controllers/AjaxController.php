<?php

/* MODELS */
use \Banner,
\Chamada,
\Destaque,
\Novidade,
\Empresa,
\Categoria,
\Produto,
\Lancamento;

/* CORE */
use \Redirect,
\Controller,
\View;

/* LIBS */
use \Tools,
\Assets,
\Thumbs;

class AjaxController extends Controller {

	protected $layout = 'frontend.templates.ajax';

	/**
	 * Setup the layout used by the controller.
	 *
	 * @return void
	 */
	protected function setupLayout()
	{
		if ( ! is_null($this->layout))
		{
			$this->layout = View::make($this->layout);			
		}
	}

	public function home(){
		$this->layout->content = View::make('frontend.home')->with('banners', Banner::orderBy('ordem', 'ASC')->get())
															->with('chamadas', Chamada::orderBy('ordem', 'ASC')->get())
															->with('destaques', Destaque::orderBy('ordem', 'ASC')->get())
															->with('novidades', Novidade::orderBy('data', 'DESC')->get());
	}

	public function empresa()
	{
		$this->layout->content = View::make('frontend.empresa.index')->with('empresa', Empresa::first());
	}

	public function produtos(){
		$this->layout->content = View::make('frontend.produtos.index')->with('categorias', Categoria::orderBy('ordem', 'asc')->get())
																	  ->with('produtos', Produto::orderBy('ordem', 'asc')->paginate(9));
	}

	public function categoria($slug_categoria = ''){
		$categoria = Categoria::where('slug', '=', $slug_categoria)->first();

		if(is_null($categoria)){
			App::abort('404');
		}

		$produtos = $categoria->produtos()->paginate(9);

		$this->layout->content = View::make('frontend.produtos.index')->with('detalheCategoria', $categoria)
																	  ->with('categorias', Categoria::orderBy('ordem', 'asc')->get())
																	  ->with('produtos', $produtos);
	}

	public function detalhes($slug_produto = ''){
		$produto = Produto::where('slug', '=', $slug_produto)->first();

		if(is_null($produto)){
			App::abort('404');
		}

		$categoria = $produto->categoria;

		$this->layout->content = View::make('frontend.produtos.detalhes')->with('detalheCategoria', $categoria)
																	  	 ->with('categorias', Categoria::orderBy('ordem', 'asc')->get())
																	  	 ->with('produto', $produto);
	}
	
	public function lancamentos(){
		$this->layout->content = View::make('frontend.lancamentos.index')->with('lancamentos', Lancamento::orderBy('ordem', 'ASC')->limit(8)->get());
	}

	public function lancamentosDetalhes($slug_lancamento = ''){
		$lancamento = Produto::where('slug', '=', $slug_lancamento)->first();

		if(is_null($lancamento)){
			App::abort('404');
		}

		$this->layout->content = View::make('frontend.lancamentos.detalhes')->with('lancamentos', Lancamento::orderBy('ordem', 'ASC')->get())
																			->with('detalhe', $lancamento);
	}
	
	public function novidades(){
		$this->layout->content = View::make('frontend.novidades.index')->with('novidades', Novidade::orderBy('data', 'DESC')->get())
																		->with('empresa', Empresa::first());
	}
	
	public function clippings(){
		$this->layout->content = View::make('frontend.clippings.index')->with('clippings', Clipping::orderBy('data', 'DESC')->get());
	}
	
	public function contato(){
		$this->layout->content = View::make('frontend.contato.index')->with('contato', Contato::first());
	}	

}

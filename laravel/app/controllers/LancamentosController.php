<?php

use \Lancamento, \Produto, \Categoria;

class LancamentosController extends BaseController {

	protected $layout = 'frontend.templates.index';

	public function index()
	{
		$this->layout->content = View::make('frontend.lancamentos.index')->with('lancamentos', Lancamento::orderBy('ordem', 'ASC')->limit(8)->get());
	}

	public function detalhes($slug_lancamento = '')
	{
		$lancamento = Produto::where('slug', '=', $slug_lancamento)->first();

		if(is_null($lancamento)){
			App::abort('404');
		}

		$this->layout->content = View::make('frontend.lancamentos.detalhes')->with('lancamentos', Lancamento::orderBy('ordem', 'ASC')->get())
																			->with('detalhe', $lancamento);
	}

}

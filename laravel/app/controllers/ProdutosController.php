<?php

use \Produto;

class ProdutosController extends BaseController {

	protected $layout = 'frontend.templates.index';

	public function index()
	{
		$this->layout->content = View::make('frontend.produtos.index')->with('categorias', Categoria::orderBy('ordem', 'asc')->get())
																	  ->with('produtos', Produto::orderBy('ordem', 'asc')->paginate(9));
	}

	public function categoria($slug_categoria = ''){
		$categoria = Categoria::where('slug', '=', $slug_categoria)->first();

		if(is_null($categoria)){
			App::abort('404');
		}

		$produtos = $categoria->produtos()->paginate(9);

		$this->layout->content = View::make('frontend.produtos.index')->with('detalheCategoria', $categoria)
																	  ->with('categorias', Categoria::orderBy('ordem', 'asc')->get())
																	  ->with('produtos', $produtos);
	}

	public function detalhes($slug_produto = ''){
		$produto = Produto::where('slug', '=', $slug_produto)->first();

		if(is_null($produto)){
			App::abort('404');
		}

		$categoria = $produto->categoria;

		$this->layout->content = View::make('frontend.produtos.detalhes')->with('detalheCategoria', $categoria)
																	  	 ->with('categorias', Categoria::orderBy('ordem', 'asc')->get())
																	  	 ->with('produto', $produto);
	}

}

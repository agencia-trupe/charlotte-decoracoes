<?php

use \Clipping;

class ClippingsController extends BaseController {

	protected $layout = 'frontend.templates.index';

	public function index()
	{
		$this->layout->content = View::make('frontend.clippings.index')->with('clippings', Clipping::orderBy('data', 'DESC')->get());
	}

}

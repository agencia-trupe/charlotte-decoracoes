<?php

use \Contato;

class BaseController extends Controller {

	protected $layout = 'frontend.templates.index';

	/**
	 * Setup the layout used by the controller.
	 *
	 * @return void
	 */
	protected function setupLayout()
	{
		if ( ! is_null($this->layout))
		{
			$this->layout = View::make($this->layout);
			$this->layout->with('contato', Contato::first());
		}
	}

}

<?php

use \Novidade, \Empresa, \Tools;

class NovidadesController extends BaseController {

	protected $layout = 'frontend.templates.index';

	public function index()
	{
		$this->layout->content = View::make('frontend.novidades.index')->with('novidades', Novidade::orderBy('data', 'DESC')->get())
																		->with('empresa', Empresa::first());
	}

}

<?php

use \Banner, \Chamada, \Destaque, \Novidade;

class HomeController extends BaseController {

	public function index()
	{
		$this->layout->content = View::make('frontend.home')->with('banners', Banner::orderBy('ordem', 'ASC')->get())
															->with('chamadas', Chamada::orderBy('ordem', 'ASC')->get())
															->with('destaques', Destaque::orderBy('ordem', 'ASC')->get())
															->with('novidades', Novidade::orderBy('data', 'DESC')->get());
	}

}

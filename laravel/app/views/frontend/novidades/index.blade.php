@section('conteudo')

	<h1 class="pagetitle">novidades</h1>

	<div class="centro">

		<div class="imagem">
			<img src="assets/images/empresa/{{$empresa->imagem}}" alt="Charlotte Movelaria">
		</div>

		<section>
			@if($novidades)
				@foreach($novidades as $novidade)
					<div class="novidade">
						<div class="superior">
							{{$novidade->titulo}}
							<div class="data">
								{{Tools::converteData($novidade->data)}}
							</div>
						</div>
						<div class="inferior">
							{{strip_tags($novidade->texto)}}
						</div>
					</div>
				@endforeach
			@endif
		</section>

		<div class="fix"></div>
	</div>
@stop

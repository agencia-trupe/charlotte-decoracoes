<!DOCTYPE html>
<html lang="pt-BR" class="no-js">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="robots" content="index, follow" />
    <meta name="author" content="Trupe Design" />
    <meta name="copyright" content="2013 Trupe Design" />
    <meta name="viewport" content="width=980,initial-scale=1">
    
    <meta name="keywords" content="" />

	<title>Charlotte Movelaria</title>
	<meta name="description" content="">
	<meta property="og:title" content="Charlotte Movelaria"/>
	<meta property="og:description" content=""/>

    <meta property="og:site_name" content="Charlotte Movelaria"/>
    <meta property="og:type" content="website"/>
    <meta property="og:image" content=""/>
    <meta property="og:url" content="{{ Request::url() }}"/>

	<base href="{{ url() }}/">
	<script>var BASE = "{{ url() }}"</script>

	@if(App::environment()=='local')

		<?=Assets::CSS(array(
			'vendor/reset-css/reset',
			'css/fontface/stylesheet',
			'vendor/nouislider/jquery.nouislider',
			'vendor/fancybox/source/jquery.fancybox',
			'css/base',
		))?>

	@else

		<?=Assets::CSS(array('css/front.concat'))?>

	@endif
	

	<script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.0/jquery.min.js"></script>
	<script>window.jQuery || document.write('<script src="{{asset('vendor/jquery/dist/jquery.min.js')}}"><\/script>')</script>	
	
	@if(App::environment()=='local')
		<?=Assets::JS(array('vendor/modernizr/modernizr', 'vendor/less.js/dist/less-1.6.2.min'))?>
	@else
		<?=Assets::JS(array('vendor/modernizr/modernizr'))?>
	@endif

	<script>
		// (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		// (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new
		// Date();a=s.createElement(o),
		// m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		// })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

		// ga('create', 'UA-', '');
		// ga('send', 'pageview');
	</script>
</head>
<body>

	<!-- Conteúdo Principal -->	
	<header>
		<nav>
			<ul>
				<li>
					<a href="{{URL::route('empresa')}}" data-slug="empresa" id="nav-empresa" title="Empresa" @if(str_is('empresa*', Route::currentRouteName())) class="ativo" @endif>Empresa</a>
				</li>
				<li>
					<a href="{{URL::route('produtos')}}" data-slug="produtos" id="nav-produtos" title="Produtos" @if(str_is('produtos*', Route::currentRouteName())) class="ativo" @endif>Produtos</a>
				</li>
				<li>
					<a href="{{URL::route('lancamentos')}}" data-slug="lancamentos" id="nav-lancamentos" title="Lançamentos" @if(str_is('lancamentos*', Route::currentRouteName())) class="ativo" @endif>Lançamentos</a>
				</li>
				<li id="link-home">
					<a href="{{URL::route('home')}}" data-slug="home" id="nav-home" title="Página Inicial"><img src="assets/images/layout/logo-charlotte-movelaria.png" alt="Charlotte Movelaria"></a>
				</li>
				<li>
					<a href="{{URL::route('novidades')}}" data-slug="novidades" id="nav-novidades" title="Novidades" @if(str_is('novidades*', Route::currentRouteName())) class="ativo" @endif>Novidades</a>
				</li>
				<li>
					<a href="{{URL::route('clippings')}}" data-slug="clippings" id="nav-clippings" title="Clipping" @if(str_is('clippings*', Route::currentRouteName())) class="ativo" @endif>Clipping</a>
				</li>
				<li>
					<a href="{{URL::route('contato')}}" data-slug="contato" id="nav-contato" title="Contato" @if(str_is('contato*', Route::currentRouteName())) class="ativo" @endif>Contato</a>
				</li>
			</ul>
		</nav>
		<div id="segundaNav">
			<div class="audioControls">
				<a href="#" title="Tocar Música" id="audio-play"><img src="assets/images/layout/botao-player-1.png" alt="Tocar Música"></a>
				<a href="#" title="Parar Música" id="audio-pause"><img src="assets/images/layout/botao-player-2.png" alt="Parar Música"></a>
				<a href="#" title="Música Anterior" id="audio-prev"><img src="assets/images/layout/botao-player-3.png" alt="Música Anterior"></a>
				<a href="#" title="Próxima Música" id="audio-next"><img src="assets/images/layout/botao-player-4.png" alt="Próxima Música"></a>
				<div id="volumecontrol"></div>
				<audio id="audio1" width="300"></audio>
			</div>
			<div class="social">
				@if($contato->twitter)
					<a href="{{$contato->twitter}}" title="Nosso Twitter" target="_blank"><img src="assets/images/layout/icone-twitter.jpg" alt="Charlotte Movelaria Twitter"></a>
				@endif
				@if($contato->facebook)
					<a href="{{$contato->facebook}}" title="Nosso Facebook" target="_blank"><img src="assets/images/layout/icone-facebook.jpg" alt="Charlotte Movelaria Facebook"></a>
				@endif
			</div>
		</div>
	</header>

	<div class="main {{ 'main-'. str_replace('-', '', str_replace('.', '', Route::currentRouteName())) }}">
		@yield('conteudo')
	</div>

	<footer>
		<div class="barraFooter"></div>
		<div class="centro">
			<div class="footer-telefone">
				@if(isset($contato->telefone))
					{{$contato->telefone}}
				@endif
			</div>
			<div class="footer-endereco">
				@if(isset($contato->endereco))
					{{strip_tags($contato->endereco)}}
				@endif
			</div>
			<div class="footer-copyright">Copyright &copy; {{Date('Y')}}</div>
		</div>
	</footer>


	<?=Assets::JS(array(
		'vendor/nouislider/jquery.nouislider.min',
		'vendor/fancybox/source/jquery.fancybox',
		'vendor/imagesloaded/imagesloaded.pkgd.min',
		'vendor/jquery.cycle/jquery.cycle.all',
		'js/main'
	))?>

</body>
</html>

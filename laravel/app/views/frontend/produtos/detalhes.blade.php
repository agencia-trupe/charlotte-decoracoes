@section('conteudo')
	
	<div class="centro">
	
		<h1 class="pagetitle">produtos</h1>

		<aside>
			<h1>Categoria</h1>
			<ul>
				<li><a href="produtos" title="Todos os Produtos" data-slug="produtos" @if(!isset($detalheCategoria)) class="ativo" @endif>Todos os produtos</a></li>
				@if($categorias)
					@foreach($categorias as $cat)
						<li><a href="produtos/categorias/{{$cat->slug}}" title="{{$cat->titulo}}" data-slug="produtos/categorias/{{$cat->slug}}" @if(isset($detalheCategoria) && $detalheCategoria->id == $cat->id) class="ativo" @endif>{{$cat->titulo}}</a></li>
					@endforeach
				@endif
			</ul>
		</aside>

		<section>
			@if($produto)
				<h1>{{$produto->categoria->titulo}}</h1>
				<div id="ampliada">
					<img src="assets/images/produtos/{{$produto->imagem}}" alt="{{$produto->titulo}}">
				</div>
				<div class="texto">
					<h2>{{$produto->prefixo}}</h2>
					<h3>{{$produto->titulo}}</h3>
					<div class="galeria">
						<a href="assets/images/produtos/{{$produto->imagem}}" title="Ver Ampliada" class="aberto">
							<img src="assets/images/produtos/thumbs/{{$produto->imagem}}" alt="{{$produto->titulo}}">
						</a>
						@if($produto->imagens)
							@foreach($produto->imagens as $imagem)
								<a href="assets/images/produtosimagens/{{$imagem->imagem}}" title="Ver Ampliada">
									<img src="assets/images/produtosimagens/thumbs/{{$imagem->imagem}}" alt="{{$produto->titulo}}">
								</a>
							@endforeach
						@endif
					</div>
					<div class="voltar">
						<a href="produtos/categorias/{{$produto->categoria->slug}}" title="Voltar">VOLTAR</a>
					</div>
				</div>
			@endif			
		</section>
	</div>
@stop
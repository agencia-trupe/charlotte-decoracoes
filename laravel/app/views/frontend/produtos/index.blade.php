@section('conteudo')
	
	<div class="centro">
	
		<h1 class="pagetitle">produtos</h1>

		<aside>
			<h1>Categoria</h1>
			<ul>
				<li><a href="produtos" title="Todos os Produtos" data-slug="produtos" @if(!isset($detalheCategoria)) class="ativo" @endif>Todos os produtos</a></li>
				@if($categorias)
					@foreach($categorias as $categoria)
						<li><a href="produtos/categorias/{{$categoria->slug}}" title="{{$categoria->titulo}}" data-slug="produtos/categorias/{{$categoria->slug}}" @if(isset($detalheCategoria) && $detalheCategoria->id == $categoria->id) class="ativo" @endif>{{$categoria->titulo}}</a></li>
					@endforeach
				@endif
			</ul>
		</aside>

		<section class="lista">
			<h1>@if(isset($detalheCategoria)) {{$detalheCategoria->titulo}} @else Todos os produtos @endif</h1>
			@if($produtos)
				@foreach($produtos as $produto)
					<a href="produtos/detalhes/{{$produto->slug}}" title="{{$produto->titulo}}" class="thumb">
						<img src="assets/images/produtos/thumbs/{{$produto->imagem}}" alt="{{$produto->titulo}}">
					</a>
				@endforeach
			@endif
			{{$produtos->links()}}
		</section>
	</div>
@stop

@section('conteudo')

	<h1 class="pagetitle">lançamentos</h1>

	<div id="faixa-branca-lanc">
		<div class="centro">
			@if($lancamentos)
				@foreach($lancamentos as $lancamento)
					<?php $produto = Produto::find($lancamento->produtos_id)?>
					<a href="lancamento/{{$produto->slug}}" title="{{$produto->titulo}}" data-slug="lancamento/{{$produto->slug}}">
						<div class="imagem">
							<img src="assets/images/produtos/thumbs/{{$produto->imagem}}" alt="{{$produto->titulo}}">
						</div>
						<div class="texto">
							{{$produto->titulo}}
						</div>
					</a>
				@endforeach
			@endif
		</div>
	</div>

@stop

@section('conteudo')
	
	<div class="centro">
	
		<h1 class="pagetitle">lançamentos</h1>

		<aside>
			<h1>Outros<br>Lançamentos</h1>
			<ul>
				@if($lancamentos)
					@foreach($lancamentos as $lanc)
						<?php $p = Produto::find($lanc->produtos_id)?>
						<li><a href="lancamento/{{$p->slug}}" title="{{$p->titulo}}" data-slug="lancamento/{{$p->slug}}" @if(isset($detalhe) && $detalhe->id == $lanc->produtos_id) class="ativo" @endif>{{$p->titulo}}</a></li>
					@endforeach
				@endif
			</ul>
		</aside>

		<section>
			<h1></h1>
			@if($detalhe)
				<div id="ampliada">
					<img src="assets/images/produtos/{{$detalhe->imagem}}" alt="{{$detalhe->titulo}}">
				</div>
				<div class="texto">
					<h2>{{$detalhe->prefixo}}</h2>
					<h3>{{$detalhe->titulo}}</h3>
					<div class="galeria">
						<a href="assets/images/produtos/{{$detalhe->imagem}}" title="Ver Ampliada" class="aberto">
							<img src="assets/images/produtos/thumbs/{{$detalhe->imagem}}" alt="{{$detalhe->titulo}}">
						</a>
						@if($detalhe->imagens)
							@foreach($detalhe->imagens as $imagem)
								<a href="assets/images/produtosimagens/{{$imagem->imagem}}" title="Ver Ampliada">
									<img src="assets/images/produtosimagens/thumbs/{{$imagem->imagem}}" alt="{{$detalhe->titulo}}">
								</a>
							@endforeach
						@endif
					</div>
					<div class="voltar">
						<a href="lancamentos" title="Voltar">VOLTAR</a>
					</div>
				</div>
			@endif			
		</section>
	</div>
@stop
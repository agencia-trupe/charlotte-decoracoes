@section('conteudo')
	
	<div class="centro topo">

		<h2>Conheça o que a Charlotte preparou especialmente para</h2>
		<h1>SEU LAR E SUA FAMÍLIA</h1>

		<div id="banners-home">
			<div class="banners">
				@if($banners)
					@foreach($banners as $banner)
						<div class="banner">
							<img src="assets/images/banners/{{$banner->imagem}}" alt="{{$banner->legenda}}">
							<div class="legenda">{{$banner->legenda}}</div>
						</div>
					@endforeach
				@endif
			</div>
			<div class="pager"></div>
		</div>

		<div id="chamadas-home">
			@if($chamadas)
				@foreach($chamadas as $chamada)
					<a href="{{$chamada->destino}}" title="{{$chamada->titulo}}" data-destino="{{$chamada->destino}}">
						<img src="assets/images/chamadas/{{$chamada->imagem}}" alt="{{$chamada->titulo}}">
						<div class="texto">{{$chamada->titulo}}</div>
					</a>
				@endforeach
			@endif
		</div>

	</div>
	
	@if($destaques)
		<div id="faixa-branca">
			
			<h1>Nossos Destaques</h1>

			<div class="centro">
				@foreach($destaques as $destaque)
					<?php $produto = Produto::find($destaque->produtos_id) ?>
					<a href="produtos/detalhes/{{$produto->slug}}" title="{{$produto->titulo}}" data-destino="produtos/detalhes/{{$produto->slug}}">
						<div class="imagem">
							<img src="assets/images/produtos/thumbs/{{$produto->imagem}}" alt="{{$produto->titulo}}">
						</div>
						<div class="texto">
							{{$produto->titulo}}
						</div>
					</a>
				@endforeach
			</div>

		</div>
	@endif

	<div class="centro inferior">
		
		<div id="coluna-novidades">

			<h1>Novidades</h1>

			@if($novidades)
				@foreach($novidades as $novidade)
					<a href="novidades" class="novidade" title="Ver mais">
						<p class="titulo">{{$novidade->titulo}}</p>
						<p class="texto">{{ Str::words(strip_tags($novidade->texto), 20)}}</p>
					</a>
				@endforeach
			@endif
			
		</div>

		<div id="coluna-newsletter">
			
			<h1>Newsletter</h1>

			<p>
				Que tal receber as últimas notícias da<br> Charlotte Movelaria em seu e-mail?
			</p>

			<form action="{{URL::route('contato.newsletter')}}" id="form-newsletter" method="post">
				<input type="text" name="nome" placeholder="Nome" id="newsletter-nome" required>
				<input type="email" name="email" placeholder="E-mail" id="newsletter-email" required>
				<div class="submit">
					<input type="submit" value="ENVIAR">
				</div>
			</form>

		</div>

	</div>

@stop

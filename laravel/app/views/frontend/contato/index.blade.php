@section('conteudo')

	<div class="centro">
		
		<h1 class="pagetitle">Contato</h1>

		<div class="contato-colunas">

			<div class="coluna-form">
				
				<h2>Mande sua Mensagem</h2>

				<form action="{{URL::route('contato.enviar')}}" id="contato-form" method="post">
					<input type="text" name="nome" placeholder="Nome" required id="contato-nome">
					<input type="email" name="email" placeholder="E-mail" required id="contato-email">
					<input type="text" name="telefone" placeholder="Telefone" id="contato-telefone">
					<textarea name="mensagem" placeholder="Mensagem" required id="contato-mensagem"></textarea>
					<input type="submit" value="ENVIAR">
				</form>

			</div>

			<div class="coluna-endereco">

				<h2>Endereço</h2>
				<div class="texto">
					{{($contato->endereco)}}
				</div>

				<h2>Telefone</h2>
				<div class="texto">
					<p class="tel">{{($contato->telefone)}}</p>
				</div>

				<h2>Horário de Atendimento</h2>
				<div class="texto">
					{{($contato->horario)}}
				</div>
				
			</div>


		</div>

	</div>
	@if($contato->google_maps)

		{{ Tools::viewGMaps($contato->google_maps, "100%", '320px') }}

	@endif
@stop

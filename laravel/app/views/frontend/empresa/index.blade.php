@section('conteudo')
	
	<div class="centro">
		
		<h1>EMPRESA</h1>
		
		<h2>Nossa história</h2>

		@if($empresa->imagem)
			<img src="assets/images/empresa/{{$empresa->imagem}}" alt="Charlotte Movelaria">
		@endif

		<div class="texto">			

			{{$empresa->texto}}

		</div>

		<div class="fix"></div>

	</div>
@stop
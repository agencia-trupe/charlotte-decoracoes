@section('conteudo')

	<div class="centro">
		<h1 class="pagetitle">clipping</h1>
		
		@if($clippings)
			<div class="lista-clippings">
				@foreach ($clippings as $clip)
					@if(sizeof($clip->paginas) > 0)
						@foreach($clip->paginas as $k => $pagina)
							@if($k == 0)
								<a href="assets/images/clippingimagens/{{$pagina->imagem}}" title="{{$clip->titulo}}" rel="galeria-{{$clip->id}}" class="link-visivel">
									<img src="assets/images/clippings/{{$clip->imagem}}" alt="{{$clip->titulo}}">
								</a>
							@endif
						@endforeach
					@else
						<a href="assets/images/clippings/{{$clip->imagem}}" title="{{$clip->titulo}}" rel="galeria-{{$clip->id}}" class="link-visivel">
							<img src="assets/images/clippings/{{$clip->imagem}}" alt="{{$clip->titulo}}">
						</a>
					@endif
				@endforeach

				@foreach ($clippings as $clip)
					@if(sizeof($clip->paginas) > 0)
						@foreach($clip->paginas as $k => $pagina)
							@if($k > 0)
								<a rel="galeria-{{$clip->id}}" title="{{$clip->titulo}}" href="assets/images/clippingimagens/{{$pagina->imagem}}" style="display:none;"></a>
							@endif
						@endforeach
					@endif
				@endforeach
			</div>
		@endif

	</div>

@stop

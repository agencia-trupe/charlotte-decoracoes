@section('conteudo')

    <div class="container add">

      	<h2>
        	Editar Lançamento
        </h2>  

		{{ Form::open( array('route' => array('painel.lancamentos.update', $registro->id), 'files' => true, 'method' => 'put') ) }}
			<div class="pad">

		    	@if(Session::has('sucesso'))
		    	   <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
		        @endif

		    	@if($errors->any())
		    		<div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
		    	@endif	


				<div class="form-group">
					<label for="inputProduto em Destaque">Produto em Destaque</label>					 
					<select	name="produtos_id" class="form-control" id="inputProduto em Destaque" required>
						<option value="">Selecione</option>
						@if($produtos)
							@foreach($produtos as $produto)
								<option value="{{$produto->id}}" @if($produto->id == $registro->produtos_id) selected @endif>{{$produto->titulo}}</option>
							@endforeach
						@endif
					</select>
				</div>

				<button type="submit" title="Alterar" class="btn btn-success">Alterar</button>

				<a href="{{URL::route('painel.lancamentos.index')}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>

			</div>
		</form>
    </div>
    
@stop
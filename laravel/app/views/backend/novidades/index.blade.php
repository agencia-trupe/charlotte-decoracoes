@section('conteudo')

<div class="container">

    @if(Session::has('sucesso'))
        <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
    @endif

    @if($errors->any())
        <div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
    @endif

    <h2>
        Novidades <a href='{{ URL::route('painel.novidades.create') }}' class='btn btn-success btn-sm pull-right'><span class='glyphicon glyphicon-plus-sign'></span> Adicionar Novidade</a>
    </h2>

    <table class='table table-striped table-bordered table-hover'>

        <thead>
            <tr>
                <th>Título</th>
				<th>Data</th>
				<th>Texto</th>
                <th><span class="glyphicon glyphicon-cog"></span></th>
            </tr>
        </thead>

        <tbody>
        @foreach ($registros as $registro)

            <tr class="tr-row" id="row_{{ $registro->id }}">
                <td>{{ $registro->titulo }}</td>
				<td>{{ Tools::converteData($registro->data) }}</td>
				<td>{{ Str::words(strip_tags($registro->texto), 15) }}</td>
                <td class="crud-actions">
                    <a href='{{ URL::route('painel.novidades.edit', $registro->id ) }}' class='btn btn-primary btn-sm'>editar</a>
						{{ Form::open(array('route' => array('painel.novidades.destroy', $registro->id), 'method' => 'delete')) }}
							<button type='submit' class='btn btn-danger btn-sm btn-delete'>excluir</button>
						{{ Form::close() }}                    
                </td>
            </tr>

        @endforeach
        </tbody>

    </table>

    
</div>

@stop
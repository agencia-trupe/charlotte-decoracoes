@section('conteudo')

    <div class="container add">

      	<h2>
        	Adicionar Destaque
        </h2>  

		<form action="{{URL::route('painel.destaques.store')}}" method="post" enctype="multipart/form-data">
			<div class="pad">

		    	@if(Session::has('sucesso'))
		    	   <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
		        @endif

		    	@if($errors->any())
		    		<div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
		    	@endif


				<div class="form-group">
					<label for="inputProduto">Produto em Destaque</label>					 
					<select	name="produtos_id" class="form-control" id="inputProduto" required>
						<option value="">Selecione</option>
						@if($produtos)
							@foreach($produtos as $produto)
								<option value="{{$produto->id}}">{{$produto->titulo}}</option>
							@endforeach
						@endif
					</select>
				</div>
				

				<button type="submit" title="Inserir" class="btn btn-success">Inserir</button>

				<a href="{{URL::route('painel.destaques.index')}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>

			</div>
		</form>
    </div>
    
@stop
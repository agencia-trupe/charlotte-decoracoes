@section('conteudo')

<div class="container">

    @if(Session::has('sucesso'))
        <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
    @endif

    @if($errors->any())
        <div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
    @endif

    <h2>
        Informações de Contato 
    </h2>

    <table class='table table-striped table-bordered table-hover'>

        <thead>
            <tr>
                <th>Facebook</th>
				<th>Twitter</th>
				<th>Endereço</th>
				<th>Telefone</th>
				<th>Horário</th>
                <th><span class="glyphicon glyphicon-cog"></span></th>
            </tr>
        </thead>

        <tbody>
        @foreach ($registros as $registro)

            <tr class="tr-row" id="row_{{ $registro->id }}">
                <td><a href="{{ $registro->facebook }}" target="_blank" class="btn btn-sm btn-default" title="{{ $registro->facebook }}">{{ $registro->facebook }}</a></td>
				<td><a href="{{ $registro->twitter }}" target="_blank" class="btn btn-sm btn-default" title="{{ $registro->twitter }}">{{ $registro->twitter }}</a></td>
				<td>{{ Str::words(strip_tags($registro->endereco), 15) }}</td>
				<td style="white-space:nowrap">{{ $registro->telefone }}</td>
                <td>{{ Str::words(strip_tags($registro->horario), 15) }}</td>
				<td class="crud-actions">
                    <a href='{{ URL::route('painel.contato.edit', $registro->id ) }}' class='btn btn-primary btn-sm'>editar</a>                    
                </td>
            </tr>

        @endforeach
        </tbody>

    </table>

    
</div>

@stop
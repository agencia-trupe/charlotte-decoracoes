@section('conteudo')

<div class="container">

    @if(Session::has('sucesso'))
        <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
    @endif

    @if($errors->any())
        <div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
    @endif

    <h2>
        Produtos @if(!is_null($categoria) && isset($categoria->titulo)) - {{$categoria->titulo}} @endif <a href='{{ URL::route('painel.produtos.create') }}' class='btn btn-success btn-sm pull-right'><span class='glyphicon glyphicon-plus-sign'></span> Adicionar Produto</a>
    </h2>

    <hr>
    @if($categorias)
        Filtrar por Categoria: 
        <div class="btn-group">
        @foreach($categorias as $cat)
            @if(!is_null($categoria) && isset($categoria->id) && $categoria->id == $cat->id)
                <a href="{{URL::route('painel.produtos.index', array('categorias_id' => $cat->id))}}" class="btn btn-sm btn-info">{{$cat->titulo}}</a>
            @else
                <a href="{{URL::route('painel.produtos.index', array('categorias_id' => $cat->id))}}" class="btn btn-sm btn-default">{{$cat->titulo}}</a>
            @endif
        @endforeach
        </div>
    @endif

    <table class='table table-striped table-bordered table-hover table-sortable' data-tabela='produtos'>

        <thead>
            <tr>
                @if(!is_null($categoria))
                    <th>Ordenar</th>
                @endif
				<th>Título</th>
                <th>Imagens</th>
                <th><span class="glyphicon glyphicon-cog"></span></th>
            </tr>
        </thead>

        <tbody>
        @foreach ($registros as $registro)

            <tr class="tr-row" id="row_{{ $registro->id }}">
                @if(!is_null($categoria))
                    <td class='move-actions'><a href='#' class='btn btn-info btn-move btn-sm'>mover</a></td>
                @endif
				<td>{{ $registro->prefixo.' - '.$registro->titulo }}</td>
                <td><a href="{{URL::route('painel.produtosimagens.index', array('produtos_id' => $registro->id))}}" title="Gerenciar Imagens do Produto" class="btn btn-default btn-sm">gerenciar</a></td>
				<td class="crud-actions">
                    <a href='{{ URL::route('painel.produtos.edit', $registro->id ) }}' class='btn btn-primary btn-sm'>editar</a>
					{{ Form::open(array('route' => array('painel.produtos.destroy', $registro->id), 'method' => 'delete')) }}
						<button type='submit' class='btn btn-danger btn-sm btn-delete'>excluir</button>
					{{ Form::close() }}
                </td>
            </tr>

        @endforeach
        </tbody>

    </table>

    
</div>

@stop
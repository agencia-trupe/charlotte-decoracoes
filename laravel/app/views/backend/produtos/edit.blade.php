@section('conteudo')

    <div class="container add">

      	<h2>
        	Editar Produto
        </h2>  

		{{ Form::open( array('route' => array('painel.produtos.update', $registro->id), 'files' => true, 'method' => 'put') ) }}
			<div class="pad">

		    	@if(Session::has('sucesso'))
		    	   <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
		        @endif

		    	@if($errors->any())
		    		<div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
		    	@endif	

				<div class="form-group">
					<label for="inputCategoria">Categoria do Produto</label>					 
					<select	name="produtos_categorias_id" class="form-control" id="inputCategoria" required>
						<option value="">Selecione</option>
						@if($categorias)
							@foreach($categorias as $categoria)
								<option value="{{$categoria->id}}" @if($categoria->id == $registro->produtos_categorias_id) selected @endif>{{$categoria->titulo}}</option>
							@endforeach
						@endif
					</select>
				</div>

				<div class="form-group">
					<label for="inputPrefixo">Prefixo</label>
					<input type="text" class="form-control" id="inputPrefixo" name="prefixo" value="{{$registro->prefixo}}" >
				</div>
				<div class="form-group">
					<label for="inputTítulo">Título</label>
					<input type="text" class="form-control" id="inputTítulo" name="titulo" value="{{$registro->titulo}}" required>
				</div>
				<div class="form-group">
					@if($registro->imagem)
						Imagem Atual<br>
						<img src="assets/images/produtos/{{$registro->imagem}}"><br>
					@endif
					<label for="inputImagem">Trocar Imagem</label>
					<input type="file" class="form-control" id="inputImagem" name="imagem">
				</div>
				

				<button type="submit" title="Alterar" class="btn btn-success">Alterar</button>

				<a href="{{URL::route('painel.produtos.index')}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>

			</div>
		</form>
    </div>
    
@stop
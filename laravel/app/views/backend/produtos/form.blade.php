@section('conteudo')

    <div class="container add">

      	<h2>
        	Adicionar Produto
        </h2>  

		<form action="{{URL::route('painel.produtos.store')}}" method="post" enctype="multipart/form-data">
			<div class="pad">

		    	@if(Session::has('sucesso'))
		    	   <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
		        @endif

		    	@if($errors->any())
		    		<div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
		    	@endif

				<div class="form-group">
					<label for="inputCategoria">Categoria do Produto</label>					 
					<select	name="produtos_categorias_id" class="form-control" id="inputCategoria" required>
						<option value="">Selecione</option>
						@if($categorias)
							@foreach($categorias as $categoria)
								<option value="{{$categoria->id}}">{{$categoria->titulo}}</option>
							@endforeach
						@endif
					</select>
				</div>

				<div class="form-group">
					<label for="inputPrefixo">Prefixo</label>
					<input type="text" class="form-control" id="inputPrefixo" name="prefixo" @if(Session::has('formulario')) value="{{ $s=Session::get('formulario'); echo $s['prefixo'] }}" @endif >
				</div>

				<div class="form-group">
					<label for="inputTítulo">Título</label>
					<input type="text" class="form-control" id="inputTítulo" name="titulo" @if(Session::has('formulario')) value="{{ $s=Session::get('formulario'); echo $s['titulo'] }}" @endif required>
				</div>

				<div class="form-group">
					<label for="inputImagem">Imagem</label>
					<input type="file" class="form-control" id="inputImagem" name="imagem" required>
				</div>
				
				<div class="form-group">
					<label><input type="checkbox" value="1" name="lancamento"> Cadastrar Produto como lançamento</label>					
				</div>

				<button type="submit" title="Inserir" class="btn btn-success">Inserir</button>

				<a href="{{URL::route('painel.produtos.index')}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>

			</div>
		</form>
    </div>
    
@stop
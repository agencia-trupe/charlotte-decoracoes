<?php

class Produto extends Eloquent
{

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'produtos';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array();

	// array of attributes we CAN set on the backend
    protected $fillable = array('', 'created_at', 'updated_at');

    // and those we CAN'T set on the backend
    protected $guarded = array('id');

    protected function categoria()
    {
    	return $this->belongsTo('Categoria', 'produtos_categorias_id');
    }

    protected function imagens()
    {
    	return $this->hasMany('ProdutosImagem', 'produtos_id');
    }
}
<?php

Route::get('teste', function(){
	echo "abc";
});

Route::get('/', array('as' => 'home', 'uses' => 'HomeController@index'));

Route::get('home', array('as' => 'home', 'uses' => 'HomeController@index'));
Route::post('home', array('as' => 'home.ajax', 'uses' => 'AjaxController@home'));

Route::get('empresa', array('as' => 'empresa', 'uses' => 'EmpresaController@index'));
Route::post('empresa', array('as' => 'empresa.ajax', 'uses' => 'AjaxController@empresa'));

Route::get('produtos', array('as' => 'produtos', 'uses' => 'ProdutosController@index'));
Route::post('produtos', array('as' => 'produtos.ajax', 'uses' => 'AjaxController@produtos'));
Route::get('produtos/categorias/{slug?}', array('as' => 'produtos.categorias', 'uses' => 'ProdutosController@categoria'));
Route::post('produtos/categorias/{slug?}', array('as' => 'produtos.categorias.ajax', 'uses' => 'AjaxController@categoria'));
Route::get('produtos/detalhes/{slug?}', array('as' => 'produtos.detalhes', 'uses' => 'ProdutosController@detalhes'));
Route::post('produtos/detalhes/{slug?}', array('as' => 'produtos.detalhes.ajax', 'uses' => 'AjaxController@detalhes'));

Route::get('lancamentos', array('as' => 'lancamentos', 'uses' => 'LancamentosController@index'));
Route::post('lancamentos', array('as' => 'lancamentos.ajax', 'uses' => 'AjaxController@lancamentos'));
Route::get('lancamento/{slug?}', array('as' => 'lancamentos.detalhes', 'uses' => 'LancamentosController@detalhes'));
Route::post('lancamento/{slug?}', array('as' => 'lancamentos.detalhes.ajax', 'uses' => 'AjaxController@lancamentosDetalhes'));

Route::get('novidades', array('as' => 'novidades', 'uses' => 'NovidadesController@index'));
Route::post('novidades', array('as' => 'novidades.ajax', 'uses' => 'AjaxController@novidades'));

Route::get('clippings', array('as' => 'clippings', 'uses' => 'ClippingsController@index'));
Route::post('clippings', array('as' => 'clippings.ajax', 'uses' => 'AjaxController@clippings'));

Route::get('contato', array('as' => 'contato', 'uses' => 'ContatoController@index'));
Route::post('contato', array('as' => 'contato.ajax', 'uses' => 'AjaxController@contato'));
Route::post('contato/enviar', array('as' => 'contato.enviar', 'uses' => 'ContatoController@enviar'));
Route::post('contato/newsletter', array('as' => 'contato.newsletter', 'uses' => 'ContatoController@newsletter'));

Route::get('painel', array('before' => 'auth', 'as' => 'painel.home', 'uses' => 'Painel\HomeController@index'));
Route::get('painel/login', array('as' => 'painel.login', 'uses' => 'Painel\HomeController@login'));

// Autenticação do Login
Route::post('painel/login',  array('as' => 'painel.auth', function(){
	$authvars = array(
		'username' => Input::get('username'),
		'password' => Input::get('password')
	);

	$lembrar = (Input::get('lembrar') == '1') ? true : false;

	if(Auth::attempt($authvars, $lembrar)){
		return Redirect::to('painel');
	}else{
		Session::flash('login_errors', true);
		return Redirect::to('painel/login');
	}
}));

Route::get('painel/logout', array('as' => 'painel.off', function(){
	Auth::logout();
	return Redirect::to('painel');
}));

Route::post('ajax/gravaOrdem', array('before' => 'auth', 'uses' => 'Painel\AjaxController@gravaOrdem'));

Route::group(array('prefix' => 'painel', 'before' => 'auth'), function()
{
    Route::resource('usuarios', 'Painel\UsuariosController');
    Route::resource('banners', 'Painel\BannersController');
	Route::resource('chamadas', 'Painel\ChamadasController');
	Route::resource('cadastros', 'Painel\CadastrosController');
	Route::get('painel/cadastros/download', array('as' => 'painel.cadastros.download', 'uses' => 'Painel\CadastrosController@download'));
	Route::resource('destaques', 'Painel\DestaquesController');
	Route::resource('empresa', 'Painel\EmpresaController');
	Route::resource('produtoscategorias', 'Painel\ProdutosCategoriasController');
	Route::resource('produtos', 'Painel\ProdutosController');
	Route::resource('produtosimagens', 'Painel\ProdutosImagensController');
	Route::resource('lancamentos', 'Painel\LancamentosController');
	Route::resource('novidades', 'Painel\NovidadesController');
	Route::resource('clippings', 'Painel\ClippingsController');
	Route::resource('clippingimagens', 'Painel\ClippingImagensController');
	Route::resource('contato', 'Painel\ContatoController');
//NOVASROTASDOPAINEL//
});
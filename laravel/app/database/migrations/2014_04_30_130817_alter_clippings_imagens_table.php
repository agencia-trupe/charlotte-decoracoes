<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterClippingsImagensTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('clippings_imagens', function(Blueprint $table)
		{
			$table->integer('clippings_id')->after('id');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('clippings_imagens', function(Blueprint $table)
		{
			$table->dropColumn('clippings_id');
		});
	}

}

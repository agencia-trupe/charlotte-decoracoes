module.exports = function(grunt) {
	
	grunt.loadNpmTasks('grunt-contrib-less');
	grunt.loadNpmTasks('grunt-contrib-concat');
	grunt.loadNpmTasks('grunt-contrib-uglify');

  	// Project configuration.
	grunt.initConfig({
		less : {
			src: '../public/assets/css/base.less',
      		dest: '../public/assets/css/base.css',
		},
		concat: {
			front: {
				files: {
					'../public/assets/css/front.concat.css' : 
					[
	    				'../public/assets/vendor/reset-css/reset.css',
	    				'../public/assets/css/fontface/stylesheet.css',
	    				'../public/assets/vendor/fancybox/source/jquery.fancybox.css',
	    				'../public/assets/vendor/nouislider/jquery.nouislider.css',
	    				'../public/assets/css/base.css'    			
	    			],
	    			'../public/assets/js/front.concat.js' :
	    			[
						'../public/assets/vendor/nouislider/jquery.nouislider.js',
						'../public/assets/vendor/fancybox/source/jquery.fancybox.js',
						'../public/assets/vendor/imagesloaded/imagesloaded.pkgd.min.js',
						'../public/assets/vendor/jquery.cycle/jquery.cycle.all.js',
						'../public/assets/js/main.js'
	    			]
	    		}
    		},
    		/*back: {
    			files: {
    				'../public/assets/css/back.concat.css' :
    				[
    					'../public/assets/vendor/reset-css/reset.css',
						'../public/assets/css/fontface/stylesheet.css',
						'../public/assets/vendor/jquery-ui/themes/base/jquery-ui.css',
						'../public/assets/vendor/bootstrap/dist/css/bootstrap.min.css',
						'../public/assets/css/painel/painel.css'
    				],
    				'../public/assets/js/back.concat.js' :
    				[
    					'../public/assets/vendor/jquery-ui/ui/minified/jquery-ui.min.js',
						'../public/assets/vendor/bootbox/bootbox.js',
						'../public/assets/vendor/ckeditor/ckeditor.js',
						'../public/assets/vendor/ckeditor/adapters/jquery.js',
						'../public/assets/vendor/bootstrap/dist/js/bootstrap.min.js',
						'../public/assets/js/painel/painel.js'
    				]
    			}
    		}*/
  		},
  		uglify: {
  			my_target:{
	    		files: {
	    			'../public/assets/js/front.min.js' : ['../public/assets/js/front.concat.js'],
	    			/*'../public/assets/js/back.min.js' : ['../public/assets/js/back.concat.js'],*/
	    		}
    		}
  		},
  	});

  	//grunt.loadNpmTasks('');
  

  	// Default task(s).
  	//grunt.registerTask('default', ['less', 'concat:front', 'concat:back', 'uglify']);
  	grunt.registerTask('default', ['less', 'concat:front', 'uglify']);

};

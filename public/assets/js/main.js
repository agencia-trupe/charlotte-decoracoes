function conteudoHandler(slug){
    $('header nav ul li a.ativo').removeClass('ativo');
    $('header nav ul li a#nav-'+slug).addClass('ativo');
    $('.main').addClass('loading');

    if(document.documentElement.scrollTop >= 215)
        $('html, body').animate({scrollTop: 0}, 100);
    
    setTimeout(function(){    
        
        $('.main').removeClass('main-home main-empresa main-produtos main-lancamentos main-novidades main-clippings main-contato')
                  .addClass('main-'+slug);

        $.post(BASE+'/'+slug, {}, function(retorno){
            $('.main').html(retorno);    
            imagesLoaded($('.main'), function(){    
                actionsHandler();    
                setTimeout(function(){
                    $('.main').removeClass('loading');
                }, 300);    
            });    
        });
    }, 300);
}

function produtosHandler(url){
    $('header nav ul li a.ativo').removeClass('ativo');
    $('header nav ul li a#nav-produtos').addClass('ativo');
    $('.main').addClass('loading');

    if(document.documentElement.scrollTop >= 215)
        $('html, body').animate({scrollTop: 0}, 100);

    setTimeout(function(){    
        
        $('.main').removeClass('main-home main-empresa main-produtos main-lancamentos main-novidades main-clippings main-contato')
                  .addClass('main-produtos');

        $.post(BASE+'/'+url.substr(2), {}, function(retorno){
            $('.main').html(retorno);    
            imagesLoaded($('.main'), function(){    
                actionsHandler();    
                setTimeout(function(){
                    $('.main').removeClass('loading');
                }, 300);    
            });    
        });
    }, 300);    
}

function lancamentosHandler(url){
    $('.main').addClass('loading');

    if(document.documentElement.scrollTop >= 215)
        $('html, body').animate({scrollTop: 0}, 100);

    setTimeout(function(){    
        
        $('.main').removeClass('main-home main-empresa main-produtos main-lancamentos main-novidades main-clippings main-contato')
                  .addClass('main-lancamentos');

        $.post(BASE+'/'+url.substr(2), {}, function(retorno){
            $('.main').html(retorno);    
            imagesLoaded($('.main'), function(){    
                actionsHandler();    
                setTimeout(function(){
                    $('.main').removeClass('loading');
                }, 300);    
            });    
        });
    }, 300);    
}

var actionsHandler = function(){
    if($('.main-home').length){
        
        $('#banners-home .banners').cycle({
            pager:  '#banners-home .pager',
            pagerAnchorBuilder: function(idx, slide) { 
                return '<a href="#"></a>';
            } 
        });

        $('#chamadas-home a').click( function(e){
            e.preventDefault();
            window.location.hash = '#!/'+$(this).attr('data-destino');
        });

        $('#faixa-branca a').click( function(e){
            e.preventDefault();
            window.location.hash = '#!/'+$(this).attr('data-destino');
        });

        $('#form-newsletter').submit( function(e){
            e.preventDefault();
            if($('#newsletter-nome').val() == '' || $('#newsletter-nome').val() == $('#newsletter-nome').attr('placeholder')){
                alert('Informe seu nome!');
                return false;
            }
            if($('#newsletter-email').val() == '' || $('#newsletter-email').val() == $('#newsletter-email').attr('placeholder')){
                alert('Informe seu e-mail!');
                return false;
            }
            $.post(BASE+'/'+"contato/newsletter", { nome : $('#newsletter-nome').val(), email : $('#newsletter-email').val() }, function(retorno){
                $('#newsletter-nome').val('');
                $('#newsletter-email').val('');
                alert(retorno);
            });
        });

        $('.novidade').click( function(e){
            e.preventDefault();
            window.location.hash = '#!/novidades';
        });

    }else if($('.main-empresa').length){
        
    }else if($('.main-produtos').length || $('.main-produtoscategorias').length || $('.main-produtosdetalhes').length){
        
        $('aside ul li a').click( function(e){
            e.preventDefault();
            window.location.hash = '#!/'+$(this).attr('data-slug');
        });

        $('.pagination li a').click( function(e){
            e.preventDefault();
            var url = $(this).attr('href').replace('/previa', '');
            url = url.split('/');
            url.splice(0, 1);
            url.splice(1, 1);
            url = url.join('/')
            url = url.replace('//', '/');
            window.location.hash = '#!'+url;
        });

        $('section.lista a.thumb').click( function(e){
            e.preventDefault();
            var url = $(this).attr('href');
            window.location.hash = '#!/'+url;
        });

        $('section .texto .galeria a').click( function(e){
            e.preventDefault();
            var href = $(this).attr('href');
            if(!$(this).hasClass('aberto')){
                $('section .texto .galeria a.aberto').removeClass('aberto');
                $(this).addClass('aberto');
                $('#ampliada img').attr('src', 'assets/images/layout/ajax-loader.gif');
                setTimeout( function(){
                    imagesLoaded($('#ampliada'), function(){
                        $('#ampliada img').attr('src', href);
                    });
                }, 300);
            }
        });

        $('section .voltar a').click( function(e){
            e.preventDefault();
            var url = $(this).attr('href');
            window.location.hash = '#!/'+url;
        });

    }else if($('.main-lancamentos').length || $('.main-lancamentosdetalhes').length){
        
        $('aside ul li a').click( function(e){
            e.preventDefault();
            window.location.hash = '#!/'+$(this).attr('data-slug');
        });

        $('section .texto .galeria a').click( function(e){
            e.preventDefault();
            var href = $(this).attr('href');
            if(!$(this).hasClass('aberto')){
                $('section .texto .galeria a.aberto').removeClass('aberto');
                $(this).addClass('aberto');
                $('#ampliada img').attr('src', 'assets/images/layout/ajax-loader.gif');
                setTimeout( function(){
                    imagesLoaded($('#ampliada'), function(){
                        $('#ampliada img').attr('src', href);
                    });
                }, 300);
            }
        });

        $('section .voltar a').click( function(e){
            e.preventDefault();
            var url = $(this).attr('href');
            window.location.hash = '#!/'+url;
        });

        $('#faixa-branca-lanc .centro a').click( function(e){
            e.preventDefault();
            window.location.hash = '#!/'+$(this).attr('data-slug');
        });

    }else if($('.main-novidades').length){

    }else if($('.main-clippings').length){
        
        $('.lista-clippings a').fancybox();

    }else if($('.main-contato').length){
        
        $('#contato-form').submit( function(e){
            e.preventDefault();

            $('#contato-form').find("input[type='submit']").attr('disabled', 'disabled');

            if($('#contato-nome').val() == '' || $('#contato-nome').val() == $('#contato-nome').attr('placeholder')){
                alert('Informe seu nome!');
                return false;
            }
            if($('#contato-email').val() == '' || $('#contato-email').val() == $('#contato-email').attr('placeholder')){
                alert('Informe seu e-mail!');
                return false;
            }
            if($('#contato-mensagem').val() == '' || $('#contato-mensagem').val() == $('#contato-mensagem').attr('placeholder')){
                alert('Informe sua mensagem!');
                return false;
            }
            $.post(BASE+'/'+'contato/enviar', {
                nome : $('#contato-nome').val(),
                email : $('#contato-email').val(),
                telefone : $('#contato-telefone').val(),
                mensagem : $('#contato-mensagem').val()
            }, function(retorno){
                $('#contato-nome').val('');
                $('#contato-email').val('');
                $('#contato-telefone').val('');
                $('#contato-mensagem').val('');
                alert(retorno);
                $('#contato-form').find("input[type='submit']").removeAttr('disabled');
            });
        });

    }
};

function locationHashChanged() {
    (window.location.hash === "#!/home") && conteudoHandler('home');
    (window.location.hash === "#!/empresa") && conteudoHandler('empresa');
    (window.location.hash === "#!/produtos") && conteudoHandler('produtos');
    (window.location.hash === "#!/lancamentos") && conteudoHandler('lancamentos');
    (window.location.hash === "#!/novidades") && conteudoHandler('novidades');
    (window.location.hash === "#!/clippings") && conteudoHandler('clippings');
    (window.location.hash === "#!/contato") && conteudoHandler('contato');
    
    (window.location.hash.substr(0, 23) === "#!/produtos/categorias/") && produtosHandler(window.location.hash);
    (window.location.hash.substr(0, 17) === "#!/produtos?page=") && produtosHandler(window.location.hash);
    (window.location.hash.substr(0, 21) === "#!/produtos/detalhes/") && produtosHandler(window.location.hash);
    (window.location.hash.substr(0, 14) === "#!/lancamento/") && lancamentosHandler(window.location.hash);    
}


$('document').ready( function(){

    /* CONTROLE DE HASH PARA NAVEGAÇÃO */
    window.onhashchange = locationHashChanged;    
    if(window.location.hash){
        locationHashChanged();        
    }

    /* CONTROLE DAS CHAMADAS DO MENU PRINCIPAL */
    $('header nav ul li a').click( function(e){
        e.preventDefault();
        window.location.hash = '#!/'+$(this).attr('data-slug');
    });

    actionsHandler();

    /* CONTROLE DE AUDIO */
	var supportsAudio = !!document.createElement('audio').canPlayType;

    if(supportsAudio) {
        //var index = 0,
        var index = Math.floor(Math.random()*(7-1+1)+1),
        playing = false;
        mediaPath = BASE+'/assets/music/',
        extension = '',
        tracks = [
            {
            	"track"	: 1,
            	"file"	: "chim_chim_cheree_mary_poppins"
            },
            {
                "track" : 2,
                "file"  : "harry_potter_melody"
            },
            {
                "track" : 3,
                "file"  : "les_eaux_de_mars"
            },
            {
                "track" : 4,
                "file"  : "ma_jeunesse"
            },
            {
                "track" : 5,
                "file"  : "proleter_-_april_showers"
            },
            {
                "track" : 6,
                "file"  : "richard_clayderman_ballade_pour_adeline"
            },
            {
                "track" : 7,
                "file"  : "shes_a_carioca"
            },
            {
                "track" : 8,
                "file"  : "so_danco_samba_jimtomlinsonstaceykent_brazilian_sketches"
            },
        ],
        trackCount = tracks.length,
        
        audio = $('#audio1').bind('play', function() {
            playing = true;            
        }).bind('pause', function() {
            playing = false;            
        }).bind('ended', function() {
            if((index + 1) < trackCount) {
                index++;
                loadTrack(index);
                audio.play();
            } else {
                audio.pause();
                index = 0;
                loadTrack(index);
            }
        }).get(0),
        btnPrev = $('#audio-prev').click(function(e) {
        	e.preventDefault();
            if((index - 1) > -1) {
                index--;
                loadTrack(index);
                if(playing) {
                    audio.play();
                }
            } else {
                audio.pause();
                index = trackCount - 1;
                loadTrack(index);
                if(playing) {
                	audio.play();
            	}
            }
        }),
        btnNext = $('#audio-next').click(function(e) {
        	e.preventDefault();
            if((index + 1) < trackCount) {
                index++;
                loadTrack(index);
                if(playing) {
                    audio.play();
                }
            } else {
                audio.pause();
                index = 0;
                loadTrack(index);
                if(playing) {
                	audio.play();
            	}
            }
        }),
        btnPlay = $('#audio-play').click( function(e){
        	e.preventDefault();
        	audio.play();
        }),
        btnPause = $('#audio-pause').click( function(e){
        	e.preventDefault();
        	audio.pause();
        }),
        loadTrack = function(id) {
            index = id;
            audio.src = mediaPath + tracks[id].file + extension;
        },
        playTrack = function(id) {
            loadTrack(id);
            audio.play();
        };
        if(audio.canPlayType('audio/ogg')) {
            extension = '.ogg';
        }
        if(audio.canPlayType('audio/mpeg')) {
            extension = '.mp3';
        }
        
        // AUTOPLAY
        playTrack(index);
        
        // SEM AUTOPLAY
        //loadTrack(index);

    }else{
    	$('.audioControls a').hide();
    }


    $('#volumecontrol').noUiSlider({
        start: 80,
        connect: "lower",
        range: {
            'min': 0,
            'max': 100
        },
        serialization: {
            lower: [
                $.Link({
                    target: function( val ){
                        document.getElementById("audio1").volume = (val / 100);
                    },
                    format: {
                        decimals: 0
                    }
                })
            ]
        }
    });

});
